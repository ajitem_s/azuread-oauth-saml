openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365

openssl pkcs12 -export -out server.pfx -inkey key.pem -in cert.pem

openssl x509 -in cert.pem -fingerprint -noout