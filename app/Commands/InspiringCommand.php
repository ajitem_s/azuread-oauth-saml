<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Firebase\JWT\JWT;

class InspiringCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'inspiring';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $certs = [];

        $pkcs12 = file_get_contents("/home/ajitem/Downloads/cert (1).pfx");

        openssl_pkcs12_read( $pkcs12, $certs, "P123" );

        $key = $certs['pkey'];
        $cert = openssl_x509_parse( $certs['cert'] );
        
        $hash = $cert['hash'];

        $base64Thumbprint = base64_encode($hash);
        $sha1Thumbprint = sha1(base64_encode($hash));
        // echo base64_encode('E402EFA998BF9ECDCCE02F9B46AFCA464CA3EE68'); dd();

        $base64Value = base64_encode($certs['cert']);

        $keyId = 'eda1f8a2-8d9b-4a5d-9fba-75ff6dc26bdd';//$this->GUID();

        $credentials['keyCredentials'] = [
            'customKeyIdentifier' => $base64Thumbprint,
            'keyId' => $keyId,
            'type' => 'AsymmetricX509Cert',
            'usage' => 'Verify',
            'value' => $base64Value
        ];

        // echo "Key Credentials: \v\n" . json_encode( $credentials );

        $payload = [
            'aud' => 'https://login.microsoftonline.com/26c7d76d-b138-4246-aae0-0f5115f02b96/oauth2/token',
            'exp' => '1924120543',
            'iss' => '257f89bc-df32-4912-a599-bf429e17fcd9',
            'jti' => 'eda1f8a2-8d9b-4a5d-9fba-75ff6dc26bdd',
            'nbf' => '1524120543',
            'sub' => '257f89bc-df32-4912-a599-bf429e17fcd9'
        ];

        $header = [
            'alg' => 'RS256',
            'typ' => 'JWT',
            'x5t' => $sha1Thumbprint
        ];
        
        $jwt = JWT::encode($payload, $certs['pkey'], 'RS256', null, $header );

       echo "JWT: " . $jwt;
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    public function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }
    
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}
