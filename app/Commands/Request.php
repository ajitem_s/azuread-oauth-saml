<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Firebase\JWT\JWT;
use App\Tasks\GetClientCredentialsWithSharedSecret;
use App\Tasks\GenerateAssertionJWT;
use App\Tasks\GetClientCredentialsWithCertificate;

class Request extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'request';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Makes request for Access Token (Client Credentials)';

    protected $tenantId = '26c7d76d-b138-4246-aae0-0f5115f02b96';

    protected $clientId = '257f89bc-df32-4912-a599-bf429e17fcd9';

    protected $clientSecret = "V9/EdT/Bb6GoAkW5vft0s+0Xb/hFAJ80Hc+eL7AfJ6s=";

    protected $clientId1 = 'f9d6e0d3-54a7-46bc-b001-456445c8d863';

    protected $clientSecret1 = '8p1ZgjT5xNBCxSN6p6Rdtl4JlmijAurUjpfTa18zYPE=';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $option = $this->menu('Azure AD OAuth Tool. Please choose OAuth Flow', [
            'Client Credentials - Shared Secret',
            'Client Credentials - JWT Assertion',
        ])->open();
        
        switch($option) {
            case 0:
                $task = new GetClientCredentialsWithSharedSecret($this->tenantId, $this->clientId, $this->clientSecret, $this->clientId1, $this->clientSecret1);
                $this->task('Get Access Token with Shared Secret', function() use ($task) {
                    return $task->do();
                });
                echo $task->getResponse();
                break;
            case 1:
                $jwtTask = new GenerateAssertionJWT();
                $this->task('Generating  Assertion JWT', function() use ($jwtTask) {
                    return $jwtTask->do();
                });
                echo 'Assertion: ' . $jwtTask->getResponse() . "\n";
        
                $clientCredentialsTask = new GetClientCredentialsWithCertificate($this->tenantId, $this->clientId,  $this->clientId1, $this->clientSecret1, $jwtTask->getResponse());
                $this->task('Get Access Token with Assertion', function() use ($clientCredentialsTask) {
                    return $clientCredentialsTask->do();
                });
                echo $clientCredentialsTask->getResponse();
                break;
            case NULL:
            default:
                exit();
        }
    }
}
