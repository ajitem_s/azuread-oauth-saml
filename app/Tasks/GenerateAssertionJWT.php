<?php

namespace App\Tasks;

use Firebase\JWT\JWT;

class GenerateAssertionJWT extends Task
{
    public function do()
    {
        $certs = [];

        $pkcs12 = file_get_contents("./cert/new/server.pfx");

        openssl_pkcs12_read( $pkcs12, $certs, "Pere@123" );

        $key = $certs['pkey'];
        $cert = openssl_x509_parse( $certs['cert'] );
        
        $payload = [
            'aud' => 'https://login.microsoftonline.com/26c7d76d-b138-4246-aae0-0f5115f02b96/oauth2/token',
            'exp' => '1924120543',
            'iss' => '257f89bc-df32-4912-a599-bf429e17fcd9',
            'jti' => '1068b188-fa8a-49e7-9460-e4b0acdc7f78',
            'nbf' => '1524120543',
            'sub' => '257f89bc-df32-4912-a599-bf429e17fcd9'
        ];

        $header = [
            'alg' => 'RS256',
            'typ' => 'JWT',
            'x5t' => 'S8SI8IBjtac8iS32Gdm3Eq0l0jQ='
        ];
        
        $this->response = JWT::encode($payload, $key, 'RS256', null, $header );

        return true;
    }
}