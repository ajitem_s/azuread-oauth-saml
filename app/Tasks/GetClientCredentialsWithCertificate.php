<?php

namespace App\Tasks;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class GetClientCredentialsWithCertificate extends Task
{
    protected $tenantId;

    protected $clientId;

    protected $clientAssertion;

    public function __construct($tenantId, $clientId, $clientAssertion)
    {
        parent::__construct();
        $this->tenantId = $tenantId;
        $this->clientId = $clientId;
        $this->clientAssertion = $clientAssertion;
    }

    public function do()
    {
        try {
            $response = $this->client->request('POST', 'https://login.microsoftonline.com/' . $this->tenantId . '/oauth2/v2.0/token', [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->clientId,
                    'client_assertion_type' => 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
                    'client_assertion' => $this->clientAssertion,
                    'scope' => 'http://aws/4eddf712-80be-4877-b80c-00150a5b3f13/.default'
                ]
            ]);
            $this->response = $response->getBody();
            return true;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $this->response = Psr7\str($e->getRequest()) . "\n\n" . Psr7\str($e->getResponse());
            }
            return false;
        }
    }
}