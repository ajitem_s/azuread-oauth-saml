<?php

namespace App\Tasks;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;

abstract class Task
{
    protected $client;

    protected $jar;

    protected $response;

    public function __construct()
    {
        $this->jar = new FileCookieJar(base_path('cookie.txt'), true);
        $this->client = new Client([
            'cookies' => $this->jar,
            // 'proxy' => '127.0.0.1:8888',
            // 'verify' => false
        ]);
    }

    public function do()
    {

    }

    public function getResponse()
    {
        return $this->response;
    }
}