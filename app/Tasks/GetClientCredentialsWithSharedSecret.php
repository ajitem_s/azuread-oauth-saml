<?php

namespace App\Tasks;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class GetClientCredentialsWithSharedSecret extends Task
{
    protected $tenantId;

    protected $clientId;

    protected $clientSecret;

    protected $clientId1;

    protected $clientSecret1;

    public function __construct($tenantId, $clientId, $clientSecret, $clientId1, $clientSecret1)
    {
        parent::__construct();
        $this->tenantId = $tenantId;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->clientId1 = $clientId1;
        $this->clientSecret1 = $clientSecret1;
    }

    public function do()
    {
        try {
            $response = $this->client->request('POST', 'https://login.microsoftonline.com/' . $this->tenantId . '/oauth2/token', [
                'cookies' => $this->jar,
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->clientId,
                    'client_secret' => $this->clientSecret,
                    'resource' => $this->clientId1,// . '/.default'
                ]
            ]);

            $this->response .= $response->getBody() . "\n\n";

            $body = json_decode($response->getBody());
            // dd($this->jar);

            $response = $this->client->request('POST', 'https://login.microsoftonline.com/' . $this->tenantId . '/oauth2/token', [
                'cookies' => $this->jar,
                'form_params' => [
                    'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                    'client_id' => $this->clientId1,
                    'client_secret' => $this->clientSecret1,
                    'resource' => $this->clientId,
                    'requested_token_use' => 'on_behalf_of',
                    'requested_token_type' => 'urn:ietf:params:oauth:token-type:saml2',
                    'assertion' => $body->access_token,
                    // 'scope' => 'f9d6e0d3-54a7-46bc-b001-456445c8d863/.default'
                ]
            ]);

            $this->response .= $response->getBody();

            return true;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $this->response .= Psr7\str($e->getRequest()) . "\n\n" . Psr7\str($e->getResponse());
            }
            return false;
        }
    }
}